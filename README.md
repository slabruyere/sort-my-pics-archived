# sort-my-pics-archived

I wrote this small program in the waiting room of a hairdresser in Kitchener in December 2014.

I wanted to have an automated way to sort my pictures.

It allowed me to sort the pictures in `pwd` in folders by year, month, day and camera (make and model).

The entry point is `findPictures.sh`.

The filetypes are hardcoded in `findPictures.sh`. An exclusion pattern (to avoid sorting some files) could be set there as well.

This program relies on the `mdls` cli available on MacOS to read files metadata.

*USE AT YOUR OWN RISK*
