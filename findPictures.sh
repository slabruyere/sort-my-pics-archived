#!/bin/bash
if [ "$1" != "" ]; then
	set -e
	if [ -f duplicates.txt ]; then
		rm duplicates.txt
	fi;

	filetypes=("jpeg" "jpg" "png" "mpg" "mov" "mpo" "mp4" "avi" "thm" "wmv")
	notPath="*Special*"

	tot="0"
	for x in "${filetypes[@]}"; do
		ct=`find . -type f -not -path "$notPath" -iname "*.$x" | wc -l`
		printf "\n$ct files found with extension .$x"
		tot=$[$tot+$ct]
	done;
	printf "\n$tot files in total\n"

	printf "\nScanning folder `pwd`"
	for x in "${filetypes[@]}"; do
		printf "\nHandling files with extension .$x"
		find . -type f -not -path "$notPath" -iname "*.$x" -exec ./moveAndRename.sh {} "$1" \;
		printf "\nAll files with extension .$x have been moved and renamed."
	done;
fi;

printf "\n"
tot="0"
for x in "${filetypes[@]}"; do
	ct=`find . -type f -not -path "$notPath" -iname "*.$x" | wc -l`
	printf "\n$ct files found with extension .$x"
	tot=$[$tot+$ct]
done;
printf "\n$tot files in total\n"

# clean up empty directories. Find can do this easily.
# Remove Thumbs.db first because of thumbnail caching
printf "\nRemoving Thumbs.db files ... "
find . -name Thumbs.db -delete
echo "done."
printf "\nRemoving desktop.ini files ... "
find . -name desktop.ini -delete
echo "done."
printf "\nRemoving .dropbox files ... "
find . -name .dropbox -delete
echo "done."
printf "\nRemoving .DS_Store files ... "
find . -name .DS_Store -delete
echo "done."
printf "\nCleaning up empty directories ... "
find . -empty -delete
printf "done.\n"