#!/bin/bash
if [[ "$1" != "" && "$2" != "" ]]; then
	fileToHandle="$1"
	firstTime="$2"

	extension=`echo ${fileToHandle##*.} | tr '[:upper:]' '[:lower:]'`
	if [ "$extension" == "jpeg" ]; then
		extension="jpg"
	fi;

	make=$(mdls -raw -name kMDItemAcquisitionMake "$fileToHandle")
	model=$(mdls -raw -name kMDItemAcquisitionModel "$fileToHandle")
	gmtDate=$(mdls -raw -name kMDItemContentCreationDate "$fileToHandle")
	gmtSeconds=`date -jf "%Y-%m-%d %H:%M:%S %z" "$gmtDate" +"%s"`

	if [ "$make" == "(null)" ]; then
		make="Unkown"
	fi;
	if [ "$model" == "(null)" ]; then
		model=""
	fi;

	tz="America/Toronto"

        # Tentatively change timezone between specific dates
	# yearCompare=`date -jf "%s" "$gmtSeconds" +"%Y%m%d"`
	# if [ $yearCompare -ge 19890706 ] || [ $yearCompare -le 19890805 ]; then
	# 	tz="Australia/Melbourne"
	# fi;

	year=`TZ=$tz date -jf "%s" "$gmtSeconds" +"%Y"`
	month=`TZ=$tz date -jf "%s" "$gmtSeconds" +"%B"`
	day=`TZ=$tz date -jf "%s" "$gmtSeconds" +"%d"`
	filename=`TZ=$tz date -jf "%s" "$gmtSeconds" +"%Y-%m-%d %H_%M_%S"`
	filename="$filename"

	cameraDir="$(echo $make $model)" 

	directory="$year/$month/$day/$cameraDir"
	if [ ! -d "$directory" ]; then
		printf "\nCreating directory $directory/ ..."
		mkdir -pv "$directory"
	fi

	fullFileName="$directory/$filename"
	fileToCreate="$fullFileName.$extension"
	if [ "$firstTime" == "true" ]; then
		if [ -f "$fileToCreate" ]; then
	    	printf "\nFile $fileToCreate already exists !"

	    	i="0"
	    	while [ -f "$fileToCreate" ]
			do
				i=$[$i+1]
				fileToCreate="$fullFileName($i).$extension"
			done
			printf "\nCreating $fullFileName($i).$extension"
			echo "$fullFileName($i).$extension" >> duplicates.txt
		fi;
	fi;
	mv -f "$fileToHandle" "$fileToCreate"

fi;
